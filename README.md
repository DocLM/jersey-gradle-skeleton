# Jersey skeleton project
Example Jersey project with Gradle

## IDE support

Gradle is configured to generate Eclipse and IntelliJ Idea project.

To generate Eclipse project run:

`gradle eclipse`

To generate IntelliJ Idea project run:

`gradle idea`

## How to run

Gradle is configured to build and generate a war file in `src/lib/`.

Use the following command to build the project: 


`gradle war`

## Tomcat deploy

Gradle is also configured to deploy the war file to a remote instance of Tomcat with text manager enabled.

Edit `build.gradle` to setup tomcat manager credentials.

To build and deploy the application run:

`gradle cargoRedeployRemote`
