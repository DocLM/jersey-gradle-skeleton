package com.doclm;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/hello")
public class HelloWorldRest {

  @GET
  public String message() {
    return "Hello, world!";
  }
}
